{-# LANGUAGE BangPatterns #-}
import Data.Function ((&))
import Data.List (foldl1', scanl')
import Data.Maybe (fromJust)
import System.Environment (getArgs)
import qualified Text.Printf as P

main :: IO ()
main = getArgs >>= aux
  where
    aux args = spinWheel 11
      & eratosthenesSieve maxFactor
      & ([2, 3, 5, 7]++)
      & takeWhile ((>) maxPrime)
      -- & map (P.printf "%i ")
      -- & foldl1' (>>)
      -- & (\m -> m >> P.printf "\n")
      & length
      & putStrLn . show
        where 
          maxPrime = read $ head args
          maxFactor = ceiling $ sqrt $ fromIntegral maxPrime :: Int

wheel :: [Int]
wheel = 2:4:2:4:6:2:6:4:2:4:6:6:2:6:4:2:6:4:6:8:4:2:4:2:4:8:6:4:6:2:4:6:2:6:6:4:2:4:6:2:6:4:2:4:2:10:2:10:wheel

spinWheel :: Int -> [Int]
spinWheel start = scanl' (+) start wheel

data PairingHeap v = Empty | Tree v [PairingHeap v] deriving (Show)

findMin :: (Ord v) => PairingHeap v -> Maybe v
findMin Empty      = Nothing
findMin (Tree v _) = Just v

-- in ML this is O(1). ~45% of the time is spent here
meld :: (Ord v) => PairingHeap v -> PairingHeap v -> PairingHeap v
meld Empty a = a
meld a Empty = a
meld heap1@(Tree v1 r1) heap2@(Tree v2 r2)
  | v1 <= v2  = Tree v1 (heap2:r1)  -- 21% time spent here
  | otherwise = Tree v2 (heap1:r2)

insert :: (Ord v) => v -> PairingHeap v -> PairingHeap v
insert v = meld $ Tree v []

mergePairs :: (Ord v) => PairingHeap v -> [PairingHeap v] -> PairingHeap v
mergePairs !acc []       = acc
mergePairs !acc [h]      = meld acc h
mergePairs !acc (f:s:tl) = mergePairs (meld acc $ meld f s) tl

deleteMin :: (Ord v) => PairingHeap v -> PairingHeap v
deleteMin Empty             = Empty
deleteMin (Tree _ subheaps) = mergePairs Empty subheaps

data Pair = Pair Int Int deriving (Show)
instance Eq Pair where (Pair _ c1) == (Pair _ c2) = c1 == c2
instance Ord Pair where (Pair _ c1) <= (Pair _ c2) = c1 <= c2

updateComposites :: Int -> PairingHeap Pair -> PairingHeap Pair
updateComposites _ Empty = Empty
updateComposites n heap@(Tree (Pair p c) _)
  | n <= c    = heap
  | otherwise = deleteMin heap & insert (Pair p (c + p)) & updateComposites n

eratosthenesSieve :: Int -> [Int] -> [Int]
eratosthenesSieve fmax ns = (map snd . filter fst) $ zip sieve ns
  where
    sieve = zipWith (\n t -> n /= lowestComposite t) ns ls
    ls = tail $ scanl' aux Empty ns
    lowestComposite = (\(Pair _ c) -> c) . fromJust . findMin  -- safe

    aux Empty n = insert (Pair n $ n * n) Empty
    aux cs    n | n == (lowestComposite cs') = cs'
                | n <= fmax                  = insert (Pair n $ n * n) cs'
                | otherwise                  = cs'
              where
                cs' = updateComposites n cs
